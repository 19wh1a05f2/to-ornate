// // import logo from './logo.svg';
// import './App.css';

// //import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

// import 'bootstrap/dist/css/bootstrap.min.css';
// import { Header, Home, DetailView, CreateView, UpdateView, Register, Navigation,  Login, Footer, Banners, Shop} from './components';
// // import {Banner} from './components';
// import { BrowserRouter, Routes, Route } from 'react-router-dom';
// import {NotFound } from './components/default';
// //import Header from './components/Header/Header';
// import DetailView from './components/details/DetailView';
// import TemplateProvider from './templates/TemplateProvider';
// import ContextProvider from './context/ContextProvider';
// import Cart from './components/cart/Cart';
// import { Box } from '@material-ui/core'

import React from 'react';
import {Box} from '@material-ui/core';
import { BrowserRouter , Routes, Route } from "react-router-dom";
import { Header, Home, DetailView, CreateView, UpdateView, Register, Navigation,  Login, Footer, Banners, Shop} from './components';
// import Navigation from './components/Navigation';
// import Home from './components/home/Home';
// import DetailView from './components/post/DetailView';
// import CreateView from './components/post/CreateView';
// import UpdateView from './components/post/UpdateView';





function App() {
  return (
    <div className="App">
        {/* <Router> */}
            <Navigation />
            <Box style={{marginTop: 64}}>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/register" element={<Register />} />
                <Route path="/login" element={<Login />} />
                <Route path="/details/:id" element={<DetailView/>} /> 
                <Route path="/create" element={<CreateView/>} />
                <Route path="/update" element={<UpdateView/>} />
            </Routes>,
            </Box>
        {/* </Router>, */}
    </div>
    
  );
}

export default App;
// import Navigation from './components/Navigation';
// import Login from './components/Login';
// import Register from './components/Register';
// import Home from './components/home/Home';
// import DetailView from './components/post/DetailView';
// import CreateView from './components/post/CreateView';
// import UpdateView from './components/post/UpdateView';
