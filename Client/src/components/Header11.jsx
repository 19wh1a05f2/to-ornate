import {AppBar, Toolbar, Typography, makeStyles} from '@material-ui/core';
import {Link} from 'react-router-dom';
const useStyles = makeStyles({
    component:{
        backgroundColor:"white",
        color:'black'
    },
    container: {
        justifyContent: 'flex-end',
        '&  > *': {
            padding: 20,
            color: 'black',
            fontSize:11,
            fontFamily: 'Basetica Light',
            textDecoration: 'none'
        }
    }
})
const Header11 = () => {
    const classes = useStyles();
    return (
        <AppBar className={classes.component}>
            <Toolbar className={classes.container}>
                <Typography>SHOP</Typography>
                <Typography>CART</Typography>
                <Typography>LOGIN</Typography>
            </Toolbar>
        </AppBar>
    )
}
export default Header11;