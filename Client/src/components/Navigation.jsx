import { AppBar, Toolbar, Typography, makeStyles, Button, MenuList } from '@material-ui/core'; 
import { Link } from 'react-router-dom';
import {Menu, Select, MenuItem, FormControl, InputLabel} from '@material-ui/core';
import {React, useState }from 'react';
import {FiChevronDown} from 'react-icons/fi'
//import { useHistory } from 'react-router-dom';
//import { useOktaAuth } from '@okta/okta-react';

const useStyle = makeStyles({
    component: {
        style:{marginTop:'50px'},
        background: '#FFFFFF',
        color: 'black'
    },
    container: {
        justifyContent: 'flex-start',
        '&  >*': {
            padding: 15,
            color: 'black',
            fontSize:15,
            marginTop:14,
            fontFamily: 'Roboto',
            textDecoration: 'none'
        },
    FormControl:{
      minWidth:50
    },
    customizeToolbar:{
       minHeight: '80px',
    }

    }
})

const Navigation = () => {
    const [anchorEl, setanchorEl] = useState(null);
    
    const openMenu = Boolean(anchorEl);

    const handleClick = (e) =>{
      setanchorEl(e.currentTarget)
    }
    const handleClose = ()=>{
      setanchorEl(null);
    }

    const classes = useStyle();

    return (
        <AppBar className={classes.component}>
            <Toolbar className={classes.container}>
            <Link to='/login' icon={<FiChevronDown/>}  aria-controls='basic-menu' aria-haspopup = "true" aria-expanded = {openMenu?"true":undefined} onClick={handleClick}> 
             LOGIN            
              </Link>
              {/* dropdown items */}
              <Menu 
                  style={{marginTop:'50px'}}
                  id="basic-menu"
                  anchorEl={anchorEl}
                  open = {openMenu}
                  onClose={handleClose}>
                <MenuItem onClick={handleClose}>Body</MenuItem>
                <MenuItem onClick={handleClose}>Mind</MenuItem>
                <MenuItem onClick={handleClose}>DIY</MenuItem>
              </Menu>

              <Link to= '/registration' aria-controls='life-style' aria-haspopup = "true" aria-expanded = {openMenu?"true":undefined} onClick={handleClick}>Registration</Link>
              <Menu 
                  style={{marginTop:'50px'}}
                  id="life-style"
                  anchorEl={anchorEl}
                  open = {openMenu}
                  onClose={handleClose}>
                <MenuItem onClick={handleClose}>Beauty</MenuItem>
                <MenuItem onClick={handleClose}>Guides</MenuItem>
                <MenuItem onClick={handleClose}>Hacks</MenuItem>
                <MenuItem onClick={handleClose}>Styles</MenuItem>
              </Menu>
                {/* <Link>{button}</Link> */}
            </Toolbar>
        </AppBar>
    )
}

export default Navigation;