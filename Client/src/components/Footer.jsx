// import React from "react";

// function Footer() {
//   return (
//     <div className="footer" style={{height:'5px', lineHeight:'5px'}}>
//       <footer class="py-5 bg-dark fixed-bottom">
//         <div class="container">
//           <p class="m-0 text-center text-white">
//             Copyright &copy; TO ORNATE
//           </p>
//         </div>
//       </footer>
//     </div>
//   );
// }

// export default Footer;


import React from "react";
import "./Footer.css";
import { AiFillInstagram } from "react-icons/ai";
import {AiFillTwitterSquare} from "react-icons/ai"
import {AiFillFacebook} from "react-icons/ai";
function Footer() {
  return (
    <div className="main-footer">
      <div className="container">
        <div className="row">
          {/* Column2 */}
          <div className="col">
            <ui className="list-unstyled">
              <li>About</li>
              <a href= "mailto:help.toornate@gmail.com">Contact</a>
            </ui>
          </div>
          {/* Column3 */}
          <div className="col">
            <ui className="list-unstyled">
              <li>Terms and Conditions</li>
              <li>Cookies Policy</li>
              <li>Accessibility</li>
            </ui>
          </div>
          <div className="col">
            <h6>Keep In Touch</h6>
            <ui className="list-unstyled">
              <li><AiFillInstagram/>Instagram</li>
              <li><AiFillFacebook/> Facebook</li>
              <li><AiFillTwitterSquare/> Twitter</li>
            </ui>
          </div>

          <div className="col-md-3 footer--col" >
            
              <img src="https://document-export.canva.com/L0KZ0/DAE7CoL0KZ0/3/thumbnail/0001.png?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAQYCGKMUHWDTJW6UD%2F20220316%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20220316T025908Z&X-Amz-Expires=10537&X-Amz-Signature=2a3e92109c54eed92ac1fb5c13ddc758d7a21c4f1333f68ed27434db36f42f85&X-Amz-SignedHeaders=host&response-expires=Wed%2C%2016%20Mar%202022%2005%3A54%3A45%20GMT" style={{height:"200px", width:"250px"}}/>
          </div>
        </div>
        <hr />
        <div className="row">
          <p className="col-sm">
            &copy;{new Date().getFullYear()} TO ORNATE | All rights reserved |
            Terms Of Service | Privacy
          </p>
        </div>
      </div>
    </div>
  );
}

export default Footer;