import { useState, useEffect } from "react";
import React from 'react';
import { useNavigate } from "react-router-dom";
export default function Login() {
    const initialValues = {email: "", password: ""};
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmit, setIsSubmit] = useState(false);
    const navigate = useNavigate();
    const handleChange = (e) => {
        const {name, value} = e.target;
        setFormValues({ ...formValues, [name]: value });
        console.log(formValues);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        setFormErrors(validate(formValues));
        setIsSubmit(true);
    };
     

    async function signIn(){
        let item={formValues}
        console.warn(item)
        try{
        let result= await fetch("http://localhost:3000/login",{
            method:'POST',
            headers:{
                "Content-Type":'application/json',
                "Accept":'application/json'
            },
            body:JSON.stringify(item)
        });
        //console.log(result.error);
        if(result.status === 404){
            alert("INVALID USERNAME OR PASSWORD") 
            navigate("/login")
        }
        else{
        //console.log(result)
        //localStorage.setItem("user-info",JSON.stringify(result));
        console.warn("result",result)
        navigate("/banner")
        }
    }catch (err) {
        console.error('err', err);
      }
    }



    useEffect(() => {
        console.log(formErrors);
        if (Object.keys(formErrors).length === 0 && isSubmit) {
            console.log(formValues);
        }
    }, [formErrors]);


    const validate = (values) => {
        const errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.email){
            errors.email = "Email is required !"
        }   else if (!regex.test(values.email)){
            errors.email = "This is an invalid email !"
        }
        if (!values.password){
            errors.password = "Password is required !"
        }   else if (values.password.length < 6){
            errors.password = "Password must be atleast 6 characters !"
        }
        return errors
    }

    return (
        <form onSubmit={handleSubmit}>

            <h3 >Log in</h3>
            {/* <div className="form-group">
                
                <input type="email" name = "email" className="form-control" placeholder="Email ID" value={formValues.email} onChange={handleChange}  style={{width: "370px"}} textAlign={'center'}/>
                <p>{formErrors.email}</p>
            </div>

            <div className="form-group" textAlign={'center'}>
               
                <input type="password" name = "password" className="form-control" placeholder="Password" value={formValues.password} onChange={handleChange} style={{width: "370px"}}  textAlign={'center'}/>
                <p>{formErrors.password}</p>
            </div> */}

        <div className="col-sm-6 offset-sm-3" style = {{marginTop:'50px'}}>
        <h2> Login Page </h2>
        <input type="email" name = "email" value={formValues.email} onChange={handleChange} className="form-control" placeholder="Email ID" style={{width:"400px"}}/>
           <p>{formErrors.email}</p>
        <br/>
        <input type="password" name = "password" value={formValues.password} onChange={handleChange} className="form-control" placeholder="Password" />
           <p>{formErrors.password}</p>
        <br/>
        </div>

            <div className="form-group">
                <div className="custom-control custom-checkbox" >
                    <input type="checkbox" className="custom-control-input" id="customCheck1"/>
                    <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                </div>
            </div>

            <button type="submit" className="btn btn-dark btn-lg btn-block" onClick={signIn} style={{width: "300px"}}>Sign in</button><br/><br/>
            <button onClick={() => navigate(-1)} className="btn btn-dark btn-lg btn-block" style={{width: "250px", height:"40px"}} >Go Back</button>
            <p align="center">
             <a href="/register" >Forgot password?</a>
            </p>
        </form>
    );
}
