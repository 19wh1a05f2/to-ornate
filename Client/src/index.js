// import React from "react";
// import ReactDOM from "react-dom";
// import "./index.css";
// import * as serviceWorker from "./serviceWorker";
// import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
// import {
//   Navigation,
//   Footer,
//   Home,
//   Register,
//   Contact,
//   Blog,
//   Posts,
//   Post,
// } from "./components";

// ReactDOM.render(
//   <Router>
//     <Navigation />
//     <Routes>
//       <Route path="/" element={<Home />} />
//       <Route path="/register" element={<Register />} />
//       <Route path="/contact" element={<Contact />} />
//     </Routes>
//     <Footer />
//   </Router>,

//   document.getElementById("root")
// );

// serviceWorker.unregister();


import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import 'bootstrap/dist/css/bootstrap.min.css';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom"

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
    <App />
    </BrowserRouter>

  </React.StrictMode>,
  document.getElementById('root')
);
reportWebVitals();